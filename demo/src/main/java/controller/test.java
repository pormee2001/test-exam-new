package controller;


import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class test {
    public static void main(String[] args) {
        {
            HttpClient httpclient = HttpClients.createDefault();

            try
            {
                URIBuilder builder = new URIBuilder("https://api.sec.or.th/FundFactsheet/fund/{proj_id}/IPO");


                URI uri = builder.build();
                HttpGet request = new HttpGet(uri);
                request.setHeader("96e2b764bf33497ea8b72092e7f3c2d7", "{4c5ec442a3c2463faa1175bb5c931cbf}");


                // Request body
                StringEntity reqEntity = new StringEntity("{body}");
//                request.setEntity(reqEntity);

                HttpResponse response = httpclient.execute(request);
                HttpEntity entity = response.getEntity();

                if (entity != null)
                {
                    System.out.println(EntityUtils.toString(entity));
                }
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
}
