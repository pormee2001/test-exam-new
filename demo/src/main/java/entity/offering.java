package entity;
@Entity
@Table(name = "offering")
public class offering {
    private String first_sell_start_date;
    private String first_sell_end_date;
    private String last_upd_date;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "project", nullable = false ,referencedColumnName="proj_id")
    private project project;
    private project proj_id;

    public entity.project getProject() {
        return project;
    }

    public void setProject(entity.project project) {
        this.project = project;
    }

    public entity.project getProj_id() {
        return proj_id;
    }

    public void setProj_id(entity.project proj_id) {
        this.proj_id = proj_id;
    }

    public String getFirst_sell_start_date() {
        return first_sell_start_date;
    }

    public void setFirst_sell_start_date(String first_sell_start_date) {
        this.first_sell_start_date = first_sell_start_date;
    }

    public String getFirst_sell_end_date() {
        return first_sell_end_date;
    }

    public void setFirst_sell_end_date(String first_sell_end_date) {
        this.first_sell_end_date = first_sell_end_date;
    }

    public String getLast_upd_date() {
        return last_upd_date;
    }

    public void setLast_upd_date(String last_upd_date) {
        this.last_upd_date = last_upd_date;
    }
}
