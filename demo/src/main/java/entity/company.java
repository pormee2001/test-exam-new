package entity;

import java.util.Set;

@Entity
@Table(name = "company")
public class company {
    @id
    private String unique_id;
    private String name_th;
    private String name_en;
    private String last_upd_date;

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<company> company;
    public company(String unique_id, String name_th, String name_en, String last_upd_date){
        this.unique_id =unique_id;
        this.name_th = name_th;
        this.name_en = name_en;
        this.last_upd_date= last_upd_date;
    }
    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getName_th() {
        return name_th;
    }

    public void setName_th(String name_th) {
        this.name_th = name_th;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getLast_upd_date() {
        return last_upd_date;
    }

    public void setLast_upd_date(String last_upd_date) {
        this.last_upd_date = last_upd_date;
    }
}
