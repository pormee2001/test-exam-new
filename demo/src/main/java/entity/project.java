package entity;

import java.util.Set;

@Entity
@Table(name = "project")
public class project {
    @Id
    private String proj_id;
    private String regis_id;
    private String regis_date;
    private String proj_name_th;
    private String proj_name_en;
    private String proj_abbr_name;
    private String fund_status;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "company", nullable = false ,referencedColumnName="unique_id")
    private company company;

    private company unique_id;
    private String permit_us_investment;
    private String invest_country_flag;
    private String last_upd_date;


    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<project> project;
    public project(String proj_id, String regis_id, String regis_date, String proj_name_th, String proj_name_en, String proj_abbr_name, String fund_status, company unique_id, String permit_us_investment, String invest_country_flag, String last_upd_date){

    }

    public String getLast_upd_date() {
        return last_upd_date;
    }

    public void setLast_upd_date(String last_upd_date) {
        this.last_upd_date = last_upd_date;
    }

    public String getProj_id() {
        return proj_id;
    }

    public void setProj_id(String proj_id) {
        this.proj_id = proj_id;
    }

    public String getRegis_id() {
        return regis_id;
    }

    public void setRegis_id(String regis_id) {
        this.regis_id = regis_id;
    }

    public String getRegis_date() {
        return regis_date;
    }

    public void setRegis_date(String regis_date) {
        this.regis_date = regis_date;
    }

    public String getProj_name_th() {
        return proj_name_th;
    }

    public void setProj_name_th(String proj_name_th) {
        this.proj_name_th = proj_name_th;
    }

    public String getProj_name_en() {
        return proj_name_en;
    }

    public void setProj_name_en(String proj_name_en) {
        this.proj_name_en = proj_name_en;
    }

    public String getProj_abbr_name() {
        return proj_abbr_name;
    }

    public void setProj_abbr_name(String proj_abbr_name) {
        this.proj_abbr_name = proj_abbr_name;
    }

    public String getFund_status() {
        return fund_status;
    }

    public void setFund_status(String fund_status) {
        this.fund_status = fund_status;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getPermit_us_investment() {
        return permit_us_investment;
    }

    public void setPermit_us_investment(String permit_us_investment) {
        this.permit_us_investment = permit_us_investment;
    }

    public String getInvest_country_flag() {
        return invest_country_flag;
    }

    public void setInvest_country_flag(String invest_country_flag) {
        this.invest_country_flag = invest_country_flag;
    }
}
